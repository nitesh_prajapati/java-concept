package com.test.multithreading;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class BlockingQueueTest {

	private static final AtomicInteger count = new AtomicInteger();
	public static BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<String>(20);

	public static void main(String[] args) {
		for (int i = 0; i < 2; i++) {
			Thread thread1 = new Thread(new Producer(blockingQueue, count));
			thread1.setName("Producer " + i);
			Thread thread2 = new Thread(new Consumer(blockingQueue));
			thread2.setName("Consumer " + i);
			thread1.start();
			thread2.start();
		}

	}
}

class Producer implements Runnable {

	private final AtomicInteger count;

	private final BlockingQueue<String> blockingQueue;

	public Producer(BlockingQueue<String> blockingQueue, AtomicInteger count) {
		this.blockingQueue = blockingQueue;
		this.count = count;
	}

	public void run() {
		System.out.println("Producer.run()");
		while (true) {
			produce();
		}
	}

	private void produce() {
		try {
			blockingQueue.put(count.getAndIncrement() + "");
			System.out.println(">>>> Putting the value in the queue: = " + count + ":: by Thread: " +
					Thread.currentThread().getName());
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

}

class Consumer implements Runnable {

	private BlockingQueue<String> blockingQueue;

	public Consumer(BlockingQueue<String> blockingQueue) {
		super();
		this.blockingQueue = blockingQueue;
	}

	@Override
	public void run() {
		System.out.println("Consumer.run() ");
		while (true) {
			consume();
		}
	}

	private void consume() {
		try {
			System.out.println("consume:  = " + blockingQueue.take());
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
