package com.nitesh.java.interview.programming;

class BinaryGap {

	public static void main(String[] args) {
		System.out.println("BinaryGap.main");
		System.out.println("Gap 1: " + findBinaryGap(1));
		System.out.println("Gap 5: " + findBinaryGap(5));
		System.out.println("Gap 9: " + findBinaryGap(9));
		System.out.println("Gap 17: " + findBinaryGap(17));
		System.out.println("Gap 20: " + findBinaryGap(20));
		System.out.println("Gap 128: " + findBinaryGap(128));
		System.out.println("Gap 529: " + findBinaryGap(529));
		System.out.println("Gap 1041: " + findBinaryGap(1041));
		System.out.println("Gap : " + findBinaryGap(594241));

	}

	private static int findBinaryGap(int number) {
		int gap = 0;
		final String binaryString = Integer.toBinaryString(number);
		System.out.println(number + " :: " + binaryString);
		final char[] chars = binaryString.toCharArray();

		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == '1') {
				for (int k = i + 1; k < chars.length; k++) {
					if (chars[k] == '1') {
						gap = k - i - 1 > gap ? k - i - 1 : gap;
						i = k;
					}
				}
			}
		}
		return gap;
	}
}

