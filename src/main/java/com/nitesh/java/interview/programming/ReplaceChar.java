package com.nitesh.java.interview.programming;

import java.util.stream.Collectors;

/*
	Complete the solution so that the function will break up camel casing, using a space between words.
	solution("camelCasing")  ==  "camel Casing"
 */
public class ReplaceChar {

	public static void main(String[] args) {
		String input = "camelCasing";
		System.out.println("camelCase(input) = " + camelCase(input));
		System.out.println("camelCaseJava8(input) = " + camelCaseJava8(input));
		System.out.println("camelCasingRegex(input) = " + camelCasingRegex(input));
	}

	private static String camelCasingRegex(String input) {
		return input.replaceAll("([A-Z])", " $1");
	}

	private static String camelCaseJava8(String str) {
		return str.chars().mapToObj(c -> c >= 'A' && c <= 'Z' ? " " + (char) c : "" + (char) c).collect(Collectors.joining());
	}

	public static String camelCase(String input) {
		final char[] chars = input.toCharArray();
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == Character.toUpperCase(chars[i])) {
				stringBuilder.append(" ").append(chars[i]);
			} else {
				stringBuilder.append(chars[i]);
			}
		}
		return stringBuilder.toString();
	}
}