package com.nitesh.java.interview.programming;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
Move the first letter of each word to the end of it, then add "ay" to the end of the word. Leave punctuation marks untouched.
pigIt('Pig latin is cool'); // igPay atinlay siay oolcay
pigIt('Hello world !');     // elloHay orldway !
 */

public class SimplePigLatin {

	public static void main(String[] args) {
		final String newString = pigIt("Pig latin is cool");
		System.out.println(newString);
		System.out.println(pigIt("Hello world [?]"));
		System.out.println(pigItStream("Hello world [?]"));
		System.out.println(pigItQuickest("Hello world [?]"));
	}

	private static String pigItQuickest(String text) {
		return text.replaceAll("(\\w)(\\w*)", "$2$1ay");
	}

	private static String pigItStream(String text) {
		return Arrays.stream(text.split(" ")).map(s -> s.matches("[a-z][A-Z]") ? s.substring(1) + s.charAt(0) + "ay" : s).collect(Collectors.joining(" "));

	}

	public static String pigIt(String text) {
		String DELIMITER = "ay";
		final List<Character> chars = Arrays.asList(new Character[] { '\'', ';', '.', '?', '!', ',', ':', '-', '_', '{', '}', '(', ')', '[', ']', '|', '"' });
		final String[] str = text.split(" ");
		StringBuilder stringBuilder = new StringBuilder();
		for (String s : str) {
			if (!chars.contains(s.charAt(0))) {
				s = s.substring(1) + s.charAt(0) + DELIMITER;
			}
			stringBuilder.append(s).append(" ");
		}
		stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(" "));
		return stringBuilder.toString();
	}

}