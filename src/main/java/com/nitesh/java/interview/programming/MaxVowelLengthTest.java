package com.nitesh.java.interview.programming;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

// The vowel substrings in the word codewarriors are o,e,a,io. The longest of these has a length of 2. Given a lowercase string that has alphabetic characters only
// (both vowels and consonants) and no spaces, return the length of the longest vowel substring. Vowels are any of aeiou.
public class MaxVowelLengthTest {

	@Test
	public void basicTests() {
		assertEquals(3, Solution.solve("ultrarevolutionariees"));
		assertEquals(2, Solution.solve("codewarriors"));
		assertEquals(3, Solution.solve("suoidea"));
		assertEquals(1, Solution.solve("strengthlessnesses"));
		assertEquals(11, Solution.solve("mnopqriouaeiopqrstuvwxyuaeiouaeiou"));
		assertEquals(11, Solution.solveJava8("mnopqriouaeiopqrstuvwxyuaeiouaeiou"));
	}
}

class Solution {

	public static int solve(String str) {
		final String[] split = str.split("[^aeiou]]");
		Arrays.stream(split).forEach(x -> System.out.println("x " + x));
		final char[] chars = str.toCharArray();
		final List<Character> vowels = Arrays.asList('a', 'i', 'e', 'o', 'u');
		int max = 0;
		for (int i = 0; i < chars.length; i++) {
			if (vowels.contains(chars[i])) {
				if (max == 0) {
					max++;
				}
				int temp = i;
				for (int k = i + 1; k < chars.length; k++) {
					if (vowels.contains(chars[k])) {
						max = Math.max(k - temp + 1, max);
						i = k;
					} else {
						break;
					}
				}
			}
		}
		return max;
	}

	public static int solveJava8(String string) {
		final String[] split = string.split("[^aieou]");
		return Stream.of(split).mapToInt(String::length).max().orElse(0);
	}
}
