package com.nitesh.java.interview.programming;

/*
Count the number of Duplicates
Write a function that will return the count of distinct case-insensitive alphabetic characters and numeric digits that occur more than once in the input string. The
input string can be assumed to contain only alphabets (both uppercase and lowercase) and numeric digits.

Example
"abcde" -> 0 # no characters repeats more than once
"aabbcde" -> 2 # 'a' and 'b'
 */

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import java.util.Map;

public class CountDuplicates {

	public static void main(String[] args) {
		System.out.println(duplicateCount("abcdabcdeef"));
	}

	private static Map<Character, Long> charFrequenciesMap(final String text) {
		final Map<Character, Long> collect = text.codePoints().map(Character::toLowerCase).mapToObj(c -> (char) c).collect(groupingBy(identity(), counting()));
		System.out.println("collect = " + collect);
		return collect;
	}

	static int duplicateCount(final String text) {
		return (int) charFrequenciesMap(text).values().stream().filter(i -> i > 1).count();
	}
}