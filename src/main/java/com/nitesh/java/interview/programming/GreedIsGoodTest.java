package com.nitesh.java.interview.programming;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

/*
https://www.codewars.com/kata/5270d0d18625160ada0000e4

Greed is a dice game played with five six-sided dice. Your mission, should you choose to accept it, is to score a
throw according to these rules. You will always be given an array with five six-sided dice values.

		Three 1's => 1000 points
		Three 6's =>  600 points
		Three 5's =>  500 points
		Three 4's =>  400 points
		Three 3's =>  300 points
		Three 2's =>  200 points
		One   1   =>  100 points
		One   5   =>   50 point

 Throw       Score
 ---------   ------------------
 5 1 3 4 1   250:  50 (for the 5) + 2 * 100 (for the 1s)
 1 1 1 3 1   1100: 1000 (for three 1s) + 100 (for the other 1)
 2 4 4 5 4   450:  400 (for three 4s) + 50 (for the 5)
*/

class Greed {

	public static int greedy(int[] ints) {
		final Map<Integer, Long> collect =
				Arrays.stream(ints).boxed().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		return collect.entrySet().stream().map(x -> findPoints(x.getKey(), x.getValue())).mapToInt(x -> x).sum();
	}

	private static int findPoints(int key, long val) {
		int points = 0;
		final int value = Math.toIntExact(val);

		switch (value) {
			case 5:
				points = calculateThreesPoint(key) + 2 * calculateOnesPoint(key);
				break;
			case 4:
				points = calculateThreesPoint(key) + calculateOnesPoint(key);
				break;
			case 3:
				points = calculateThreesPoint(key);
				break;
			case 2:
				points = 2 * calculateOnesPoint(key);
				break;
			case 1:
				points = calculateOnesPoint(key);
				break;
		}
		return points;
	}

	private static int calculateOnesPoint(int key) {
		int points = 0;
		switch (key) {
			case 1:
				points = 100;
				break;
			case 5:
				points = 50;
				break;
		}
		return points;
	}

	private static int calculateThreesPoint(int key) {
		int points = 0;
		switch (key) {
			case 1:
				points = 1000;
				break;
			case 2:
				points = 200;
				break;
			case 3:
				points = 300;
				break;
			case 4:
				points = 400;
				break;
			case 5:
				points = 500;
				break;
			case 6:
				points = 600;
				break;
		}
		return points;
	}
}

public class GreedIsGoodTest {

	@Test
	public void testExample() {
		assertEquals(250, Greed.greedy(new int[] { 5, 1, 3, 4, 1 }), "Score for [5,1,3,4,1] must be 250:");
		assertEquals(1100, Greed.greedy(new int[] { 1, 1, 1, 3, 1 }), "Score for [1,1,1,3,1] must be 1100:");
		assertEquals(450, Greed.greedy(new int[] { 2, 4, 4, 5, 4 }), "Score for [2,4,4,5,4] must be 450:");
	}
}