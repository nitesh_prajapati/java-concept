package com.nitesh.java.interview.programming;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MakeAnagramTest {

	public static void main(String[] args) {
		assertThat(makeAnagram("fcrxzwscanmligyxyvym", "jxwtrhvujlmrpdoqbisbwhmgpmeoke")).isEqualTo(30);
		assertThat(makeAnagram("abc", "bde")).isEqualTo(4);
	}

	private static long makeAnagram(String str1, String str2) {
		final Map<Character, Long> str1Map = str1.chars().mapToObj(x -> (char) x)
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		final Map<Character, Long> str2Map = str2.chars().mapToObj(x -> (char) x)
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

		System.out.println("str1Map = " + str1Map);
		System.out.println("str2Map = " + str2Map);
		long count = 0;
		final Set<Character> characters = str1Map.keySet();
		for (Character character1 : characters) {
			if (str2Map.containsKey(character1)) {
				long i = str1Map.get(character1) - str2Map.get(character1);
				if (i < 0) {
					i = i * -1;
				}
				count = count + i;
			} else {
				count = count + str1Map.get(character1);
			}
		}

		for (Character character2 : str2Map.keySet()) {
			if (!str1Map.containsKey(character2)) {
				count = count + str2Map.get(character2);
			}
		}
		return count;
	}
}

