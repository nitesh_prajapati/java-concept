package com.nitesh.java.concept.serialization;

import java.lang.reflect.Constructor;

// Break the singleton using reflection

class EagerInitializedSingleton {

	private static final EagerInitializedSingleton instance = new EagerInitializedSingleton();

	// private constructor to avoid client applications to use constructor
	private EagerInitializedSingleton() {
	}

	public static EagerInitializedSingleton getInstance() {
		return instance;
	}

	private Object readResolve() {
		System.out.println("EagerInitializedSingleton.readResolve()");
		return instance;
	}

	public void printName() {
		System.err.println("EagerInitializedSingleton.printName()");
	}
}

public class SingletonReflection {

	public static void main(String[] args) {
		EagerInitializedSingleton instanceOne = EagerInitializedSingleton.getInstance();
		EagerInitializedSingleton instanceTwo = null;
		try {
			Constructor[] constructors = EagerInitializedSingleton.class.getDeclaredConstructors();
			for (Constructor constructor : constructors) {
				//Below code will destroy the singleton pattern
				constructor.setAccessible(true);
				instanceTwo = (EagerInitializedSingleton) constructor.newInstance();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		instanceOne.printName();
		instanceTwo.printName();
		System.out.println(instanceOne.hashCode());
		System.out.println(instanceTwo.hashCode());
	}
}