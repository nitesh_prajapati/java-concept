package com.nitesh.java.concept.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class MyNameComparator implements Comparator<Person> {

	@Override
	public int compare(Person o1, Person o2) {
		System.out.println("MyNameComparator.compare()" + o1 + o2);
		System.out.println(o1.getName().compareTo(o2.getName()));
		return o1.getName().compareTo(o2.getName());
	}
}

public class TestComparator {

	public static void main(String[] args) {
		List<Person> persons = new ArrayList<Person>();
		Person person1 = new Person(12, "nitesh");
		Person person2 = new Person(2, "Kumar");
		Person person3 = new Person(3, "rahul");
		Person person4 = new Person(12, "nitesh");
		System.out.println(person1.equals(person4));

		//		Set<Person> set = new HashSet<>();
		//		set.add(person4);
		//		set.add(person1);
		//		System.out.println(set);

		persons.add(person1);
		persons.add(person2);
		persons.add(person3);
		persons.add(person4);

		System.err.println(persons);

		Collections.sort(persons);

		System.out.println(persons);

		Collections.sort(persons, new MyNameComparator());
		//		System.err.println(persons);

		final MyNameComparator myNameComparator = new MyNameComparator();
		Collections.sort(persons, myNameComparator);

		System.out.println("MyNameComparator: " + persons);

		// Java 7
		Collections.sort(persons, new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.name.compareTo(o2.name);
			}
		});

		// Java 8 Lambda expression
		Collections.sort(persons, (o1, o2) -> o1.name.compareTo(o2.name));
	}

}
