package com.nitesh.java.concept.collection;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SortingMapByValueAndKey {

	public static void main(String[] args) {
		Map<String, Integer> map = new HashMap<>();
		map.put("Neha", 2);
		map.put("Nitesh", 2);
		map.put("Nihaan", 2);
		map.put("Golu", 3);
		map.put("Gola", 3);

		System.out.println("Map: " + map);

		Map<String, Integer> linkedMap = new LinkedHashMap<>();

		final List<Map.Entry<String, Integer>> list = map.entrySet().stream().sorted((o1, o2) -> {
			final int compareValue = o1.getValue() - o2.getValue();
			if (compareValue == 0) {
				return o1.getKey().compareTo(o2.getKey());
			}
			return compareValue;
		}).collect(Collectors.toList());

		for (Map.Entry<String, Integer> entry : list) {
			linkedMap.put(entry.getKey(), entry.getValue());
		}

		System.out.println("Sorted map by value and then by key:: " + linkedMap.toString());
	}
}