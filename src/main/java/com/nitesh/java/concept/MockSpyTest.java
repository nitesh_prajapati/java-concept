package com.nitesh.java.concept;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MockSpyTest {

	@Mock
	private List<String> mockList = new ArrayList<>();

	@Spy
	private List<String> spyList = new ArrayList<>();

	@Test
	public void testMockList() {
		//by default, calling the methods of mock object will do nothing
		mockList.add("test");
		assertThat(mockList.get(0)).isNull();
	}

	@Test
	public void testMockWithStub() {
		when(mockList.get(0)).thenReturn("nitesh");
		assertThat(mockList.get(0)).isEqualTo("nitesh");
	}

	@Test
	public void testSpyList() {
		//spy object will call the real method when not stub
		spyList.add("test");
		assertThat(spyList.get(0)).isEqualTo("test");
	}

	@Test
	public void testSpyListWithStub() {
		//spy object will call the real method when not stub
		doReturn("test").when(spyList).get(0);
		assertThat(spyList.get(0)).isEqualTo("test");
	}


}