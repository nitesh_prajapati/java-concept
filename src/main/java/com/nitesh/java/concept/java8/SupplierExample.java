package com.nitesh.java.concept.java8;

import java.util.function.Supplier;

public class SupplierExample {

	public static void main(String[] args) {
		Supplier<Double> supplier = () ->  Math.random();
		System.out.println("supplier = " + supplier.get());

	}
}
