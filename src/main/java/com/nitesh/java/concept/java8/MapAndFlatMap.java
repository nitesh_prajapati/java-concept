package com.nitesh.java.concept.java8;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

class MapAndFlatMap {

	public static void main(String[] args) {
		// Reference: https://www.baeldung.com/java-difference-map-and-flatmap
		// Reference: https://www.techiedelight.com/difference-map-flatmap-java/
		// Difference between map() and flatMap()
		// flatMap() flattens the stream
		// It maps to the stream (x-> x.stream())

		final Optional<String> str = Optional.of("test");
		assertThat(Optional.of("TEST")).isEqualTo(str.map(String::toUpperCase));

		//	Nested structure Optional<Optional<String>>.
		//  Although it works, it's pretty cumbersome to use and does not provide any additional null-safety,
		assertThat(Optional.of(Optional.of("TEST"))).isEqualTo(Optional.of("test").map(s -> Optional.of("TEST")));

		// Flat maps reduces the extra optional
		assertThat(Optional.of("TEST")).isEqualTo(Optional.of("test").flatMap(s -> Optional.of("TEST")));

		// Lets take an example of with Arraylist
		final List<List<String>> lists = Arrays.asList(Arrays.asList("A", "B"), Arrays.asList("C", "D"));

		System.out.println("lists = " + lists);
		System.out.println("FlatMap = " + lists.stream().flatMap(Collection::stream).collect(Collectors.toList()));

	}

}


