package com.nitesh.java.concept.java8;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.DoublePredicate;
import java.util.function.IntPredicate;
import java.util.function.LongPredicate;
import java.util.function.Predicate;

/*
 	->	Predicate: It accepts one argument and returns true or false
  		-> IntPredicate
  		-> LongPredicate
  		-> DoublePredicate
 */
public class PredicateExample {

	public static void main(String[] args) {

		Predicate<Integer> predicate1 = (i) -> i > 10;
		System.out.println(predicate1.test(9));

		Predicate<Integer> predicate2 = (i) -> i % 2 == 0;

		System.out.println(predicate1.and(predicate2).test(14));

		Predicate<Instructor> predicate3 = (p) -> p.isOnlineCourses();
		Predicate<Instructor> predicate4 = (p) -> p.getYearOfExperience() > 10;
		System.out.println("------------------------------------------------------");

		final List<Instructor> instructors = Instructors.getAll();
		instructors.stream().filter(predicate3).forEach(instructor -> System.out.println(instructor));

		System.out.println("------------------------------------------------------");
		instructors.forEach(s -> {
			if (predicate3.and(predicate4.negate()).test(s)) {
				System.out.println(s);
			}
		});

		// IntPredicate
		IntPredicate intPredicate = (i) -> i < 100;
		System.out.println(intPredicate.test(95));

		// LongPredicate
		LongPredicate longPredicate = (i) -> i < 100L;
		System.out.println(longPredicate.test(95));

		// DoublePredicate
		DoublePredicate doublePredicate = (i) -> i < 100.05;
		System.out.println(doublePredicate.test(95));

		// BiPredicate
		BiPredicate<Boolean, Integer> biPredicate = (s1, s2) -> s1 && s2 > 10;
		BiConsumer<String, List<String>> biConsumer = (s1, s2) -> System.out.println("Name: " + s1 + " and courses: " + s2);

		instructors.forEach(s -> {
			if (biPredicate.test(s.isOnlineCourses(), s.getYearOfExperience())) {
				biConsumer.accept(s.getName(), s.getCourses());
			}
		});

	}
}
