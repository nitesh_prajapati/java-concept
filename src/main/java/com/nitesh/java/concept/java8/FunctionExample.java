package com.nitesh.java.concept.java8;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/*
	->	Function: It takes one arguments and return a resutl
	-> BiFunction: It takes two arguments and return a result
 */
public class FunctionExample {

	public static void main(String[] args) {
		Function<String, String> function1 = s -> s.toLowerCase();
		Function<String, String> function2 = s -> s + " IS MY SON";

		String name = "Nihaan";
		System.out.println(function1.apply(name));
		// andThen() it will apply funtion1 first and then function2
		System.out.println(function1.andThen(function2).apply(name));

		// compose() it will apply funtion2 first and then function1
		System.out.println(function1.compose(function2).apply(name));

		// Return a map with name and their experience
		Function<List<Instructor>, Map<String, Integer>> mapFunction = instructors -> {
			Map<String, Integer> map = new HashMap<>();
			instructors.forEach(i -> map.put(i.getName(), i.getYearOfExperience()));
			return map;
		};

		final List<Instructor> instructors = Instructors.getAll();
		System.out.println(mapFunction.apply(instructors));

		BiFunction<List<Instructor>, Integer, Map <String, Integer>> biFunction = ((s1, s2) -> {
			Map<String, Integer> map = new HashMap<>();
			s1.forEach(s -> {
				if (s.getYearOfExperience() >= s2) {
					map.put(s.getName(), s.getYearOfExperience());
				}
			});
			return map;
		});
		System.out.println(biFunction.apply(instructors, 10));

	}
}
