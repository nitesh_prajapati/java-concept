package com.nitesh.java.concept.java8;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;
import java.util.function.IntConsumer;
import java.util.function.LongConsumer;

/*
Consumer example
	-> Consumer: Accept one argument and produces a result but doesn't return it.
		-> IntConsumer
		-> LongConsumer
		-> DoubleConsumer
	-> BiConsumer: Accept two arguments and produces a result but doesn't return it.
 */
public class ConsumerExample {

	public static void main(String[] args) {
		System.out.println("ConsumerExample.main");

		Consumer consumer1 = (s) -> System.out.println("Value passed is: " + s);
		consumer1.accept("Nihaan");
		consumer1.accept(2L);

		Consumer<Integer> consumer2 = i -> System.out.println(i * 2);
		consumer2.accept(5);

		// Two consumer one after another accepting the same value
		consumer1.andThen(consumer2).accept(10);

		final List<Instructor> instructors = Instructors.getAll();
		// Print all the instructors
		instructors.forEach(consumer1);
		instructors.forEach((s) -> System.out.println(s));

		System.out.println("------------------------------------------------------");
		// Print only the instructor's name
		Consumer<Instructor> name = instructor -> System.out.println(instructor.getName());
		instructors.forEach(name);
		instructors.forEach(s -> System.out.println(s.getName()));
		System.out.println("------------------------------------------------------");

		// Print only the instructor's name and their courses
		Consumer<Instructor> courses = instructor -> System.out.println(instructor.getCourses());
		instructors.forEach(courses);
		instructors.forEach(s -> System.out.println(s.getCourses()));
		System.out.println("------------------------------------------------------");

		// Print name with courses
		instructors.forEach(name.andThen(courses));
		System.out.println("------------------------------------------------------");

		// Print instructor's name with experience greater than 10
		instructors.forEach(s -> {
			if (s.getYearOfExperience() > 10) {
				System.out.println(s.getName());
			}
		});
		System.out.println("------------------------------------------------------");

		IntConsumer intConsumer = (value) -> {
			System.out.println("IntConsumer: square of " + value + " :: " + value * value);
		};
		intConsumer.accept(2);

		LongConsumer longConsumer = (value) -> {
			System.out.println("LongConsumer: square of " + value + " :: " + value * value);
		};
		longConsumer.accept(2L);

		DoubleConsumer doubleConsumer = (value) -> {
			System.out.println("DoubleConsumer: square of " + value + " :: " + value * value);
		};
		doubleConsumer.accept(1.5);
		System.out.println("------------------------------------------------------");

		BiConsumer<Integer, Integer> biConsumer = (x, y) -> System.out.println(x + y);
		biConsumer.accept(4, 5);
		System.out.println("------------------------------------------------------");

		// Print the name and gender
		BiConsumer<String, String> biConsumer1 = (s1, s2) -> {
			System.out.println("s1: " + s1 + " s2: " + s2);
		};
		instructors.forEach(s -> biConsumer1.accept(s.getName(), s.getGender()));
		System.out.println("------------------------------------------------------");

		// Print the name and gender who are online instructor
		instructors.forEach(s -> {
			if (s.isOnlineCourses()) {
				biConsumer1.accept(s.getName(), s.getGender());
			}
		});

	}

}
