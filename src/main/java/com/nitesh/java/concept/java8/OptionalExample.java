package com.nitesh.java.concept.java8;

import java.util.Optional;

public class OptionalExample {

	public static void main(String[] args) {
		System.out.println("Optional.of(\"Hello Nihaan\") = " + Optional.of("Hello Nihaan"));

		try {
			System.out.println("Optional.of(null) = " + Optional.of(null));
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		System.out.println("Optional.empty() = " + Optional.empty());
		System.out.println("Optional.ofNullable(\"Hello Nihaan\" = " + Optional.ofNullable("Hello Nihaan"));

		System.out.println("Optional.empty() = " + Optional.empty());

		final Optional<String> stringOptional = Optional.ofNullable("Hello Nihaan");
		final String optionalValue = stringOptional.orElse("Hello Nitesh");
		System.out.println("optionalValue = " + optionalValue);
		System.out.println("orElse = " + Optional.ofNullable(null).orElse("Hello Nitesh"));
		System.out.println("orElseGet = " + Optional.ofNullable(null).orElse("Hello Nitesh"));
		try {
			System.out.println("orElseThrow = " + Optional.ofNullable(null).orElseThrow(Exception::new));
		} catch (Exception e) {
			e.printStackTrace();
		}

		final Optional<String> optional = Optional.ofNullable("Hello Nihaan");
		if (optional.isPresent()) {
			System.out.println("optional = " + optional.get());
		}

		optional.ifPresent(s -> System.out.println("optional.get() = " + optional.get()));

	}
}
