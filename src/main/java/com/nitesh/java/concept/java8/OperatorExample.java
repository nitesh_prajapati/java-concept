package com.nitesh.java.concept.java8;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.function.IntUnaryOperator;
import java.util.function.LongBinaryOperator;
import java.util.function.LongUnaryOperator;
import java.util.function.UnaryOperator;

/*
	->	UnaryOperator: It is a type of function which operates on one operand and return the result of the same type as of operand
		-> IntUnaryOperator
		-> LongUnaryOperator
		-> DoubleUnaryOperator
 */
public class OperatorExample {

	public static void main(String[] args) {
		// UnaryOperator
		UnaryOperator<Integer> unaryOperator = (i) -> i * 10;
		System.out.println(unaryOperator.apply(5));

		Function<Integer, Integer> function = i -> i * 100;
		System.out.println(function.apply(5));

		IntUnaryOperator intUnaryOperator = IntUnaryOperator.identity();
		System.out.println(intUnaryOperator.applyAsInt(11));

		IntUnaryOperator intUnaryOperator1 = i -> i * 1000;
		System.out.println("intUnaryOperator1.applyAsInt(12) = " + intUnaryOperator1.applyAsInt(12));

		LongUnaryOperator longUnaryOperator = l -> l * 11;
		System.out.println("longUnaryOperator.applyAsLong(11L) = " + longUnaryOperator.applyAsLong(11L));

		DoubleUnaryOperator doubleUnaryOperator = d -> d * 10;
		System.out.println("doubleUnaryOperator.applyAsDouble(10.5) = " + doubleUnaryOperator.applyAsDouble(10.5));

		BinaryOperator<Integer> binaryOperator = (a, b) -> a + b;
		System.out.println("binaryOperator.apply(10,5) = " + binaryOperator.apply(10, 5));

		BiFunction<Integer, Integer, Integer> biFunction = (a, b) -> a - b;
		System.out.println("biFunction.apply(10, 5) = " + biFunction.apply(10, 5));

		IntBinaryOperator intBinaryOperator = (a, b) -> a * b;
		System.out.println("intBinaryOperator.applyAsInt(10, 5) = " + intBinaryOperator.applyAsInt(10, 5));

		LongBinaryOperator longBinaryOperator = (a, b) -> a * b;
		System.out.println("longBinaryOperator.applyAsLong(10L, 5L) = " + longBinaryOperator.applyAsLong(10L, 5L));

		DoubleBinaryOperator doubleBinaryOperator = (a, b) -> a * b;
		System.out.println("doubleBinaryOperator.applyAsDouble(10.5, 5.5) = " + doubleBinaryOperator.applyAsDouble(10.5, 5.5));
	}
}
