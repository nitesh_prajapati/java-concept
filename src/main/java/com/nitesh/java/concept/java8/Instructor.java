package com.nitesh.java.concept.java8;

import java.util.Arrays;
import java.util.List;

public class Instructor {

	private String name;
	private int yearOfExperience;
	private String title;
	private String gender;
	private boolean onlineCourses;
	private List<String> courses;

	public Instructor(String name, int yearOfExperience, String title, String gender, boolean onlineCourses, List<String> courses) {
		this.name = name;
		this.yearOfExperience = yearOfExperience;
		this.title = title;
		this.gender = gender;
		this.onlineCourses = onlineCourses;
		this.courses = courses;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYearOfExperience() {
		return yearOfExperience;
	}

	public void setYearOfExperience(int yearOfExperience) {
		this.yearOfExperience = yearOfExperience;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(boolean onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

	public List<String> getCourses() {
		return courses;
	}

	public void setCourses(List<String> courses) {
		this.courses = courses;
	}

	@Override
	public String toString() {
		return "Instructor{" + "name='" + name + '\'' + ", yearOfExperience=" + yearOfExperience + ", title='" + title + '\'' + ", gender='" + gender + '\'' +
				", onlineCourses=" + onlineCourses + ", courses=" + courses + '}';
	}
}

class Instructors {

	public static List<Instructor> getAll() {
		return Arrays.asList(new Instructor("Mike", 10, "Software Developer", "M", true, Arrays.asList("Java", "C++", "Python")),
				new Instructor("Jenny", 5, "Engineer", "F", false, Arrays.asList("MultiThread", "CI/CD")),
				new Instructor("Syed", 15, "Senior Developer", "M", true, Arrays.asList("Angular", "React")),
				new Instructor("Syed", 5, "Senior Developer", "M", true, Arrays.asList("Java", "React")));
	}
}