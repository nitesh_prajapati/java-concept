package com.nitesh.java.concept.java8;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/*
	-> Collectors: This provides many useful reduction operations
		->> Joining
			->>> joining()
			->>> joining(delimiter)
			->>> joining(delimiter, prefix, suffix)
		->> Counting
		->> Mapping
			->>> mapping(Function, Collectors): It is the most useful collectors when used in a multi-level reduction.
			->>> flatMapping(Function, Collectors)
		->> MinBy(Comparator)
		->> MaxBy(Comparator)
		->> SummingInt(ToIntFunction)
		->> AverageInt(ToIntFunction)
		->> GroupingBy
			->>> groupBY(Function)
			->>> groupBy(Function, Collectors)
			->>> groupBy(Function, Supplier, Collectors)
				// Example for below are missing
				->>> groupingByConcurrent(Function)
				->>> groupingByConcurrent(Function, Collectors)
				->>> groupingByConcurrent(Function, Supplier, Collectors)
		->> PartitioningBy
			->>> partitioningBy(Predicate)
			->>> partitioningBy(Predicate, Collectors)
 */
public class CollectorsExample {

	public static void main(String[] args) {
		final List<Instructor> instructors = Instructors.getAll();
		System.out.println(instructors);

		// Joining
		System.out.println(instructors.stream().map(s -> s.getName()).collect(Collectors.joining()));
		System.out.println(instructors.stream().map(s -> s.getName()).collect(Collectors.joining(",")));
		System.out.println(instructors.stream().map(s -> s.getName()).collect(Collectors.joining(", ", "{ ", " }")));
		System.out.println("------------------------------------------------------");

		// Counting
		System.out.println(instructors.stream().collect(Collectors.counting()));
		System.out.println("------------------------------------------------------");

		// Mapping: It accepts new function and another collector.
		// Below two methods are doing the same thing but mapping is useful when you have downstream operations
		System.out.println(instructors.stream().map(s -> s.getName()).collect(Collectors.toList()));
		System.out.println(instructors.stream().collect(Collectors.mapping(s -> s.getName(), Collectors.toList())));
		System.out.println(instructors.stream().collect(Collectors.groupingBy(s -> s.getYearOfExperience(), Collectors.mapping(s -> s.getName(), Collectors.toList()))));
		System.out.println("------------------------------------------------------");

		// FlatMapping()
		System.out.println("FlatMapping() = " + instructors.stream()
				.collect(Collectors.groupingBy(s -> s.getYearOfExperience(), Collectors.flatMapping(s -> s.getCourses().stream(), Collectors.toSet()))));
		System.out.println("------------------------------------------------------");

		// MinBy and MaxBy: It accepts a comparator
		System.out.println(instructors.stream().min(Comparator.comparing(s -> s.getYearOfExperience())));
		System.out.println(instructors.stream().collect(Collectors.minBy(Comparator.comparing(s -> s.getYearOfExperience()))));
		System.out.println(instructors.stream()
				.collect(Collectors.filtering(s -> s.getName().toLowerCase().contains("m"), Collectors.minBy(Comparator.comparing(s -> s.getYearOfExperience())))));
		System.out.println("------------------------------------------------------");

		// SummingInt
		System.out.println("summingInt() = " + instructors.stream().collect(Collectors.summingInt(s -> s.getYearOfExperience())));
		System.out.println("------------------------------------------------------");

		// AverageInt
		System.out.println("averagingInt() = " + instructors.stream().collect(Collectors.averagingInt(s -> s.getYearOfExperience())));
		System.out.println("------------------------------------------------------");

		// GroupingBy
		System.out.println(instructors.stream().collect(Collectors.groupingBy(s -> s.getYearOfExperience())));
		System.out.println(instructors.stream().collect(Collectors.groupingBy(s -> s.getYearOfExperience(), Collectors.mapping(s -> s.getName(), Collectors.toList()))));
		System.out.println(instructors.stream().collect(Collectors.groupingBy(s -> s.getYearOfExperience() > 10 ? "Senior" : "Junior",
				Collectors.filtering(s -> s.getName().toLowerCase().contains("m"), Collectors.mapping(s -> s.getName(), Collectors.toList())))));
		System.out.println("------------------------------------------------------");

		// Partitioning
		System.out.println(instructors.stream().collect(Collectors.partitioningBy(s -> s.getGender().toUpperCase().equals("M"))));
		System.out.println(instructors.stream().collect(Collectors.partitioningBy(s -> s.isOnlineCourses(), Collectors.mapping(s -> s.getName(), Collectors.toList()))));
	}
}
