package com.nitesh.java.concept.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamExample {

	public static void main(String args[]) {
		System.out.println("Using Java 7: ");

		// Count empty strings
		List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd", "", "jkl");
		System.out.println("List: " + strings);
		long count = getCountEmptyStringUsingJava7(strings);

		System.out.println("Empty Strings count using java 7: " + count);
		System.out.println("Empty Strings count using java 8: " + strings.stream().filter(s -> s.isEmpty()).count());
		System.out.println("Reverse order" + strings.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
		System.out.println("Uppercase " + strings.stream().map(s -> s.toUpperCase()).collect(Collectors.toList()));

		strings.forEach(s -> System.out.print(s + " "));

		final Iterator<String> iterator = strings.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}

		//Eliminate empty string
		List<String> filtered = deleteEmptyStringsUsingJava7(strings);
		System.out.println("Filtered List using java 7: " + filtered);
		System.out.println("Filtered List using java 8: " + strings.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList()));

		//Eliminate empty string and join using comma.
		String mergedString = getMergedStringUsingJava7(strings, ", ");
		System.out.println("Merged String using java 7: " + mergedString);
		System.out.println("Merged String using java 8: " + strings.stream().filter(s -> !s.isEmpty()).collect(Collectors.joining(", ")));

		//get list of square of distinct numbers
		List<Integer> numbers = Arrays.asList(2, 3, 4, 5, 6, 7, 8);
		List<Integer> squaresList = getSquares(numbers);
		System.out.println("Squares List using Java 7: " + squaresList);
		System.out.println("Squares List using Java 8: " + numbers.stream().map(i -> i * i).collect(Collectors.toList()));

		List<Integer> integers = Arrays.asList(1, 2, 13, 4, 25, 6, 17, 8, 19);
		final IntSummaryStatistics statistics = integers.stream().mapToInt(x -> x).summaryStatistics();

		System.out.println("List: " + integers);
		System.out.println("Highest number in List using java 7: " + getMax(integers));
		System.out.println("Highest number in List using java 8: " + statistics.getMax());
		System.out.println("Lowest number in List using java 7: " + getMin(integers));
		System.out.println("Lowest number in List using java 8: " + statistics.getMin());
		System.out.println("Sum of all numbers using java 7: " + getSum(integers));
		System.out.println("Sum of all numbers using java 8: " + statistics.getSum());
		System.out.println("Average of all numbers using java 7: " + getAverage(integers));
		System.out.println("Average of all numbers using java 8: " + statistics.getAverage());
	}

	private static int getCountEmptyStringUsingJava7(List<String> strings) {
		int count = 0;

		for (String string : strings) {

			if (string.isEmpty()) {
				count++;
			}
		}
		return count;
	}

	private static List<String> deleteEmptyStringsUsingJava7(List<String> strings) {
		List<String> filteredList = new ArrayList<String>();

		for (String string : strings) {

			if (!string.isEmpty()) {
				filteredList.add(string);
			}
		}
		return filteredList;
	}

	private static String getMergedStringUsingJava7(List<String> strings, String separator) {
		StringBuilder stringBuilder = new StringBuilder();

		for (String string : strings) {

			if (!string.isEmpty()) {
				stringBuilder.append(string);
				stringBuilder.append(separator);
			}
		}
		String mergedString = stringBuilder.toString();
		return mergedString.substring(0, mergedString.length() - 2);
	}

	private static List<Integer> getSquares(List<Integer> numbers) {
		List<Integer> squaresList = new ArrayList<>();

		for (Integer number : numbers) {
			int square = number * number;

			if (!squaresList.contains(square)) {
				squaresList.add(square);
			}
		}
		return squaresList;
	}

	private static int getMax(List<Integer> numbers) {
		int max = numbers.get(0);

		for (int i = 1; i < numbers.size(); i++) {

			Integer number = numbers.get(i);

			if (number.intValue() > max) {
				max = number;
			}
		}
		return max;
	}

	private static int getMin(List<Integer> numbers) {
		int min = numbers.get(0);

		for (int i = 1; i < numbers.size(); i++) {
			Integer number = numbers.get(i);

			if (number < min) {
				min = number;
			}
		}
		return min;
	}

	private static int getSum(List numbers) {
		int sum = (int) (numbers.get(0));

		for (int i = 1; i < numbers.size(); i++) {
			sum += (int) numbers.get(i);
		}
		return sum;
	}

	private static int getAverage(List<Integer> numbers) {
		return getSum(numbers) / numbers.size();
	}
}