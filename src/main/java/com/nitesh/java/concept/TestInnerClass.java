package com.nitesh.java.concept;

class TestInnerClass {

	protected class Inner {

		public void printMessage(String string) {
			System.out.println("inner.main() " + string);
		}
	}

	public static class StaticInner {

		private void printMsg(String msg) {
			System.out.println(msg);
		}
	}

	public static void main(String[] args) {
		TestInnerClass testInnerClass = new TestInnerClass();
		Inner inner = testInnerClass.new Inner();
		inner.printMessage("asdf");

		Inner i = testInnerClass.new Inner();
		i.printMessage("ds");

		new StaticInner().printMsg("F");

	}
}
